package com.example.dto;

import java.io.Serializable;

public class HorasTrabajadas implements Serializable {

	private int idWorked;
	private int employeId;
	private int workeHour;
	private String workedDate;

	public int getIdWorked() {
		return idWorked;
	}

	public void setIdWorked(int idWorked) {
		this.idWorked = idWorked;
	}

	public int getEmployeId() {
		return employeId;
	}

	public void setEmployeId(int employeId) {
		this.employeId = employeId;
	}

	public int getWorkeHour() {
		return workeHour;
	}

	public void setWorkeHour(int workeHour) {
		this.workeHour = workeHour;
	}

	public String getWorkedDate() {
		return workedDate;
	}

	public void setWorkedDate(String workedDate) {
		this.workedDate = workedDate;
	}

	@Override
	public String toString() {
		return "HorasTrabajadas [idWorked=" + idWorked + ", employeId=" + employeId + ", workeHour=" + workeHour
				+ ", workedDate=" + workedDate + "]";
	}

}
