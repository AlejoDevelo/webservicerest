package com.example.dto;

import java.io.Serializable;

public class InserEmpleado implements Serializable{

	private int IDEMPLEADO;
	private int IDGENDER;
	private int JOBID;
	private String NOMBRE;
	private String APELLIDO;
	private String NACIMIENTO;
	public int getIDEMPLEADO() {
		return IDEMPLEADO;
	}
	public void setIDEMPLEADO(int iDEMPLEADO) {
		IDEMPLEADO = iDEMPLEADO;
	}
	public int getIDGENDER() {
		return IDGENDER;
	}
	public void setIDGENDER(int iDGENDER) {
		IDGENDER = iDGENDER;
	}
	public int getJOBID() {
		return JOBID;
	}
	public void setJOBID(int jOBID) {
		JOBID = jOBID;
	}
	public String getNOMBRE() {
		return NOMBRE;
	}
	public void setNOMBRE(String nOMBRE) {
		NOMBRE = nOMBRE;
	}
	public String getAPELLIDO() {
		return APELLIDO;
	}
	public void setAPELLIDO(String aPELLIDO) {
		APELLIDO = aPELLIDO;
	}
	public String getNACIMIENTO() {
		return NACIMIENTO;
	}
	public void setNACIMIENTO(String nACIMIENTO) {
		NACIMIENTO = nACIMIENTO;
	}
	@Override
	public String toString() {
		return "InserEmpleado [IDEMPLEADO=" + IDEMPLEADO + ", IDGENDER=" + IDGENDER + ", JOBID=" + JOBID + ", NOMBRE="
				+ NOMBRE + ", APELLIDO=" + APELLIDO + ", NACIMIENTO=" + NACIMIENTO + "]";
	}

	

}
