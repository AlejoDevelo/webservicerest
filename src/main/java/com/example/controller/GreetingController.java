package com.example.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.Empleado;
import com.example.dto.Greeting;
import com.example.dto.HorasTrabajadas;
import com.example.dto.InserEmpleado;
import com.example.service.ServiceCallImple;

@RestController
public class GreetingController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@Autowired
	private ServiceCallImple employeeService;

	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	//invoke http://localhost:8080/employees?id=1&idGender=1&idJob=1&nombre=test&apellido=testdos&fechaNac=1992-01-01
	@GetMapping("/employees")
	public String createEmployee(@RequestParam(value = "id") int id,
			                  @RequestParam(value = "idGender") int idGender,
			                  @RequestParam(value = "idJob") int idJob,
			                  @RequestParam(value = "nombre") String nombre,
			                  @RequestParam(value = "apellido") String apellido,
			                  @RequestParam(value = "fechaNac") String fechaNac) {
		
		InserEmpleado empleado = new InserEmpleado();
		empleado.setIDEMPLEADO(id);
		empleado.setIDGENDER(idGender);
		empleado.setJOBID(idJob);
		empleado.setNOMBRE(nombre);
		empleado.setAPELLIDO(apellido);
		empleado.setNACIMIENTO(fechaNac);
		System.out.println("onjecto: "+empleado.toString());
		return employeeService.insertarEmpleado(empleado);
		
	}
	
	//invoke http://localhost:8080/horastrabajadas?idWork=6&idEmpleado=1&horas=20&fecha=2000-01-03
	@GetMapping("/horastrabajadas")
	public String createHorasTrabajadas(@RequestParam(value = "idWork") int idWork,
			                  @RequestParam(value = "idEmpleado") int idEmpleado,
			                  @RequestParam(value = "horas") int horasW,
			                  @RequestParam(value = "fecha") String fecha) throws ParseException {
		
		HorasTrabajadas horas = new HorasTrabajadas();
		horas.setIdWorked(idWork);
		horas.setEmployeId(idEmpleado);
		horas.setWorkeHour(horasW);
		horas.setWorkedDate(fecha);
		System.out.println("objeto creacion de horas: "+horas.toString());
		return employeeService.insertarHorasTrabajadas(horas);
		
	}
	
	//http://localhost:8080/obtenEmpleado?idJob=1
	@GetMapping("/obtenEmpleado")
	public List<Empleado> createHorasTrabajadas(@RequestParam(value = "idJob") int idJob)  {
		System.out.println("id del puesto "+idJob);
		return employeeService.getEmpleado(idJob);
		
	}
	
	//http://localhost:8080/obtenHoras?idEmpleado=1&fechaInicio=01-06-20&fechaFin=26-06-20
	@GetMapping("/obtenHoras")
	public int cuantasHoras(@RequestParam(value = "idEmpleado") int idEmpleado,
									 @RequestParam(value = "fechaInicio")String fechaInicio,
									 @RequestParam(value = "fechaFin")String fechaFin)  {
		System.out.println("id del puesto "+idEmpleado);
		return employeeService.getHorasTrabajadas(idEmpleado, fechaInicio, fechaFin);
		
	}
	
	//http://localhost:8080/obtenHoras?idEmpleado=1&fechaInicio=01-06-20&fechaFin=26-06-20
	@GetMapping("/obtenPago")
	public int cuantasPagar(@RequestParam(value = "idEmpleado") int idEmpleado,
									 @RequestParam(value = "fechaInicio")String fechaInicio,
									 @RequestParam(value = "fechaFin")String fechaFin)  {
		System.out.println("id empleado "+idEmpleado);
		return employeeService.getPagoTrabajadas(idEmpleado, fechaInicio, fechaFin);
		
	}
}
