package com.example.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.dto.Empleado;
import com.example.dto.HorasTrabajadas;
import com.example.dto.InserEmpleado;

import oracle.jdbc.OracleTypes;

@Repository("categoryDaoImpl")
public class EmpleadoDAOImple implements EmpleadoDAO {

	@Override
	public String insertarEmpleado(InserEmpleado insert) {
		String runSP = "{ call llamarservicio.insertaempleado(?,?,?,?,?,?,?) }";
		Connection con = null;
		CallableStatement callableStatement = null;

		try {

			con = Jdbc.getConnection();
			callableStatement = con.prepareCall(runSP);
			System.out.println("Salida dAO empleado : " + insert.toString());
			callableStatement.setInt(1, insert.getIDEMPLEADO());
			callableStatement.setInt(2, insert.getIDGENDER());
			callableStatement.setInt(3, insert.getJOBID());
			callableStatement.setString(4, insert.getNOMBRE());
			callableStatement.setString(5, insert.getAPELLIDO());
			callableStatement.setString(6, insert.getNACIMIENTO());
			callableStatement.registerOutParameter(7, OracleTypes.VARCHAR);

			callableStatement.executeUpdate();

			String msjSalida = callableStatement.getString(7);
			System.out.println("Salida : " + msjSalida);

			return msjSalida;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public String insertarHorasTrabajadas(HorasTrabajadas horas) {
		String runSP = "{ call llamarservicio.insertarhorastrabajadas(?,?,?,?,?) }";
		Connection con = null;
		CallableStatement callableStatement = null;

		try {

			con = Jdbc.getConnection();
			callableStatement = con.prepareCall(runSP);
			System.out.println("Salida horaras trabajadas : " + horas.toString());
			callableStatement.setInt(1, horas.getIdWorked());
			callableStatement.setInt(2, horas.getEmployeId());
			callableStatement.setInt(3, horas.getWorkeHour());
			callableStatement.setString(4, horas.getWorkedDate());
			callableStatement.registerOutParameter(5, OracleTypes.VARCHAR);
			
			callableStatement.executeUpdate();

			String msjSalida = callableStatement.getString(5);
			System.out.println("Salida : " + msjSalida);
			return msjSalida;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public List<Empleado> getEmpleado(int idJob) {
		String runSP = "{ call llamarservicio.selectempleado(?,?,?) }";
		Connection con = null;
		CallableStatement callableStatement = null;
		ResultSet rset;
		List<Empleado> lisEmp = new ArrayList<Empleado>();
		try {

			con = Jdbc.getConnection();
			callableStatement = con.prepareCall(runSP);
			System.out.println("empleadoss : " + idJob);
			callableStatement.setInt(1, idJob);
			callableStatement.registerOutParameter(2, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(3, OracleTypes.CURSOR);

			callableStatement.executeUpdate();

			rset = (ResultSet) callableStatement.getObject(3);

			if (rset == null) {
				System.out.println("Problema con la BD");
			} else {
				

				while (rset.next()) {
					Empleado empleado = new Empleado();
					empleado.setId(rset.getInt(1));
					empleado.setNombre(rset.getString(2));
					empleado.setApellido(rset.getString(3));
					empleado.setCumpleaños(rset.getString(4));
					empleado.setPuesto(rset.getString(5));
					empleado.setSalario(rset.getFloat(6));
					empleado.setSexo(rset.getString(7));
					lisEmp.add(empleado);
				}

				String msjSalida = callableStatement.getString(2);
				System.out.println("mesnaje : " + msjSalida);
			}
			callableStatement.close();
			return lisEmp;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			e.printStackTrace();

			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int getHorasTrabajadas(int idEmpleado,String fechainicio,String fechaFin) {
		String runSP = "{ call llamarservicio.selecthoras(?,?,?,?) }";
		Connection con = null;
		CallableStatement callableStatement = null;
		
		try {

			con = Jdbc.getConnection();
			callableStatement = con.prepareCall(runSP);
			System.out.println("select a horas : " + idEmpleado);
			callableStatement.setInt(1, idEmpleado);
			callableStatement.setString(2, fechainicio);
			callableStatement.setString(3, fechaFin);
			callableStatement.registerOutParameter(4, java.sql.Types.INTEGER);

			callableStatement.executeUpdate();

			int msjSalida = callableStatement.getInt(4);
			System.out.println("Salida : " + msjSalida);
			return msjSalida;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			e.printStackTrace();

			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int getPagoTrabajadas(int idEmpleado,String fechainicio,String fechaFin) {
		String runSP = "{ call llamarservicio.selectpago(?,?,?,?) }";
		Connection con = null;
		CallableStatement callableStatement = null;

		try {

			con = Jdbc.getConnection();
			callableStatement = con.prepareCall(runSP);
			System.out.println("select a horas : " + idEmpleado);
			callableStatement.setInt(1, idEmpleado);
			callableStatement.setString(2, fechainicio);
			callableStatement.setString(3, fechaFin);
			callableStatement.registerOutParameter(4, java.sql.Types.INTEGER);

			callableStatement.executeUpdate();

			int msjSalida = callableStatement.getInt(4);
			System.out.println("Salida : " + msjSalida);
			return msjSalida;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			e.printStackTrace();

			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

}
