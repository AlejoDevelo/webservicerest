package com.example.service;

import java.sql.Date;
import java.util.List;

import com.example.dto.Empleado;
import com.example.dto.HorasTrabajadas;
import com.example.dto.InserEmpleado;

public interface ServiceCall {

	String insertarEmpleado(InserEmpleado insert);

	String insertarHorasTrabajadas(HorasTrabajadas horas);

	List<Empleado> getEmpleado(int idJob);

	int getHorasTrabajadas(int idEmpleado,String fechainicio,String fechaFin);

	int getPagoTrabajadas(int idEmpleado,String fechainicio,String fechaFin);
}
