package com.example.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.EmpleadoDAO;
import com.example.dto.Empleado;
import com.example.dto.HorasTrabajadas;
import com.example.dto.InserEmpleado;

@Service("categoriaServicio")
public class ServiceCallImple implements ServiceCall {

	@Autowired
	private EmpleadoDAO dao;

	@Override
	public String insertarEmpleado(InserEmpleado insert) {
		return dao.insertarEmpleado(insert);
	}

	@Override
	public String insertarHorasTrabajadas(HorasTrabajadas horas) {
		return dao.insertarHorasTrabajadas(horas);
	}

	@Override
	public List<Empleado> getEmpleado(int idJob) {
		return dao.getEmpleado(idJob);
	}

	@Override
	public int getHorasTrabajadas(int idEmpleado,String fechainicio,String fechaFin) {
		return dao.getHorasTrabajadas(idEmpleado, fechainicio, fechaFin);
	}

	@Override
	public int getPagoTrabajadas(int idEmpleado,String fechainicio,String fechaFin) {
		return dao.getPagoTrabajadas(idEmpleado, fechainicio, fechaFin);
	}

}
